using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HitFace : MonoBehaviour
{
    [Header("Values")]
    public List<GameObject> faces = new List<GameObject>();
    public int activeFace = 0;
    public TextMeshPro healthDamage;
    public int health = 3;

    [Header("Prefabs")]
    public GameObject prfExplode;
    public Transform compExplodePosition;

    void Start()
    {

    }

    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Item"))
        {
            ItemBase item = other.GetComponent<ItemBase>();
            item.Kill();

            GetDamage();
        }
    }

    public void GetDamage()
    {
        if (health - 1 <= 0)
        {
            Dead();
        }
        else
        {
            health--;
            ChangeFace();
            UpdateTXT();
        }

        Instantiate(prfExplode, compExplodePosition.position, Quaternion.identity);
    }

    public void ChangeFace()
    {
        faces[activeFace].SetActive(false);
        activeFace++;
        faces[activeFace].SetActive(true);
    }

    public void UpdateTXT()
    {
        healthDamage.text = health.ToString();
    }

    public void Dead()
    {
        Destroy(gameObject);
    }

}
