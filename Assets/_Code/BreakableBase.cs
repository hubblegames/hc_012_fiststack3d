using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RayFire;
using _Code.StarterPack.Camera;
using DG.Tweening;

public class BreakableBase : MonoBehaviour
{
    [Header("Values")]
    public RayfireRigid breakbleItem;
    public ItemType breakByType;


    void Start()
    {
        breakbleItem = GetComponentInChildren<RayfireRigid>();
    }

    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Item"))
        {
            ItemBase item = other.GetComponent<ItemBase>();

            if (((int)breakByType) <= (int)item.ItemType)
            {
                breakbleItem.Demolish();
                item.Kill();

                Destroy(gameObject);
            }
            else
            {
                item.Kill();
            }
        }
    }
}
