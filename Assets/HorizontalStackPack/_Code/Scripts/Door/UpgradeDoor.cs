using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeDoor : DoorBase
{
    [Header("Value")]
    public int moneyValue = 0;

    [Header("Component")]
    public GameObject compTransparentIcon;

    public override void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Item"))
        {
            if (compTransparentIcon.activeInHierarchy)
                compTransparentIcon.SetActive(false);

            ItemBase item = other.GetComponent<ItemBase>();

            if (Time.time - item.lastUpgradeTime >= 0.5)
            {
                item.UpgradeItem();
                itemPasses++;
                CreateDolares(itemPasses);
            }
        }
    }


}
