using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SellDoor : DoorBase
{
    [Header("Components")]
    public Transform moveTransform;
    public Transform moveEndTransform;

    void Start()
    {

    }

    void Update()
    {

    }

    public override void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Item"))
        {
            ItemBase item = other.GetComponent<ItemBase>();
            item.Deactivate();

            DOTween.Sequence()
                .Append(item.transform.DOMove(moveTransform.position, 0.4f))
                .AppendCallback(() => item.transform.DOMove(moveEndTransform.position, 3f));

            Player.ins.compMoneyContainer.FindWorth();
        }
    }

}
