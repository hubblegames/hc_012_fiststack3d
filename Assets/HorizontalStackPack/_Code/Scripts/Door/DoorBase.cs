using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorBase : MonoBehaviour
{

    [Header("Components")]
    public Transform compDolaresPos;
    public int itemPasses = 0;

    [Header("Prefabs")]
    public GameObject prfDolarTXT;

    void Start()
    {

    }

    void Update()
    {

    }

    public virtual void OnTriggerEnter(Collider other)
    {

    }

    public virtual void CreateDolares(int values)
    {
        Dolares dolares = Instantiate(prfDolarTXT, compDolaresPos.position, Quaternion.identity).GetComponent<Dolares>();
        dolares.SetText(values);
    }
}
