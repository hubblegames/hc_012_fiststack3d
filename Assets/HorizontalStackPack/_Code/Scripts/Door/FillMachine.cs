using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillMachine : DoorBase
{
    [Header("Value")]
    public int moneyValue = 2;

    public override void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Item"))
        {
            ItemBase item = other.GetComponent<ItemBase>();

            if (item.IsFilled())
            {
                item.itemListAppearance[(int)item.ItemType].FillItem();

                item.ScalePunch();

                item.AddMoneyValue(moneyValue);

                itemPasses++;
                CreateDolares(itemPasses * moneyValue);
            }
        }
    }
}
