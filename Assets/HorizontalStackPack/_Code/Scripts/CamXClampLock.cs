using UnityEngine;
using Cinemachine;

public class CamXClampLock : CinemachineExtension
{
    [Tooltip("Lock the camera's X position clamped in -/+")]
    public float m_XPosition = 0;

    protected override void PostPipelineStageCallback(
        CinemachineVirtualCameraBase vcam,
        CinemachineCore.Stage stage, ref CameraState state, float deltaTime)
    {
        if (stage == CinemachineCore.Stage.Body)
        {
            var pos = state.RawPosition;
            pos.x = Mathf.Clamp(pos.x, -m_XPosition, m_XPosition);
            state.RawPosition = pos;
        }
    }
}