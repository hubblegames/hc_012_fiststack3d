using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ChildItemBase : MonoBehaviour
{

    [Header("Values")]
    public bool isFilled = false;
    public Vector3 vec;

    [Header("Components")]
    public GameObject itemFill;
    public ItemBase itemBase;

    void Start()
    {

    }

    void Update()
    {

    }

    public void ResetRotation()
    {
        transform.DORotate(Vector3.zero, 0.2f, RotateMode.Fast);
    }

    public void FillItem()
    {
        if (isFilled)
            return;

        isFilled = true;

        itemFill.SetActive(true);
    }
}
