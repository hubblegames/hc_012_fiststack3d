using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;

public class ItemBase : MonoBehaviour
{
    [Header("Values")]
    public ItemType ItemType;
    public bool isActive = false;
    public float speed;
    public float offsetZ;
    public float lastUpgradeTime;
    public int moneyValue = 5;
    public bool isFirst = false;

    [Header("Components")]
    public Transform compItemRef;
    public Collider compCollider;

    [Header("Prefabs")]
    public GameObject prfPuff;

    [Header("Item List")]
    public GameObject activeItemAppereance;
    public List<ChildItemBase> itemListAppearance = new List<ChildItemBase>();

    public virtual void Start()
    {
        activeItemAppereance = itemListAppearance[0].gameObject;
        compCollider = GetComponent<Collider>();
    }

    public virtual void Activate()
    {
        isActive = true;
    }

    public void Deactivate()
    {
        isActive = false;
        StackManager.ins.stackItems.Remove(this);
        compItemRef = null;
        compCollider.enabled = false;
    }

    public virtual void Update()
    {
        if (isActive)
            Move();
    }

    public virtual void Move()
    {
        if (compItemRef == null)
            return;

        if (isFirst)
        {
            transform.position = compItemRef.position;
        }
        else
        {
            Vector3 refPos = new Vector3(compItemRef.position.x, compItemRef.position.y, compItemRef.position.z + offsetZ);
            transform.position = Vector3.Lerp(transform.position, refPos, Time.deltaTime * speed);
        }
    }

    public virtual void UpgradeItem()
    {
        lastUpgradeTime = Time.time;
        ItemType++;
        ScalePunch();
        ChangeItemAppearance(ItemType);
    }

    public void ChangeItemAppearance(ItemType itemType)
    {
        activeItemAppereance.SetActive(false);
        itemListAppearance[(int)itemType].gameObject.SetActive(true);
        activeItemAppereance = itemListAppearance[(int)itemType].gameObject;
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        if (!isActive)
            return;

        if (other.transform.CompareTag("Item"))
        {
            ItemBase item = other.GetComponent<ItemBase>();

            if (item.isActive)
                return;

            SetRefItem(item);

            StackManager.ins.ReArrange();
        }
    }

    public void SetRefItem(ItemBase item)
    {
        ItemBase lastItem = StackManager.ins.stackItems.Last();
        item.compItemRef = lastItem.transform;

        item.Activate();

        item.ResetRotation();

        StackManager.ins.stackItems.Add(item);

        StackManager.ins.PunchScaleAll();

        Player.ins.compMoneyContainer.FindWorth();
    }

    public void SetRefItem(ItemBase item, Transform playerRefPos)
    {
        item.compItemRef = playerRefPos.transform;

        item.Activate();

        item.ResetRotation();

        StackManager.ins.stackItems.Add(item);

        Player.ins.compMoneyContainer.FindWorth();
    }

    public void MoveThere(Vector3 movePos)
    {
        transform.DOMove(movePos, 0.4f);
    }

    public virtual void Kill()
    {
        StackManager.ins.stackItems.Remove(this);
        StackManager.ins.ReArrange();

        Instantiate(prfPuff, transform.position, Quaternion.identity);

        Player.ins.compMoneyContainer.FindWorth();

        Destroy(gameObject);
    }

    public void AddMoneyValue(int value)
    {
        moneyValue += value;
        Player.ins.compMoneyContainer.FindWorth();
    }

    public bool IsFilled()
    {
        if (ItemType != ItemType.First)
            return false;

        if (itemListAppearance[0].GetComponent<ChildItemBase>().isFilled)
            return false;
        else
            return true;

    }

    public void ResetRotation()
    {
        activeItemAppereance.GetComponent<ChildItemBase>().ResetRotation();
    }

    public void ScalePunch()
    {
        transform.DOPunchScale(Vector3.one * 0.8f, 0.2f, 5, 1);
    }

}
