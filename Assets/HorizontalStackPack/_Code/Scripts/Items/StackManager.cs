using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StackManager : MonoBehaviour
{
    #region --------------------| Ins

    private static StackManager _ins;

    public static StackManager ins
    {
        get
        {
            if (_ins == null)
                _ins = FindObjectOfType<StackManager>();
            return _ins;
        }
    }

    #endregion

    public List<ItemBase> stackItems = new List<ItemBase>();

    [Header("Values")]
    public float lastPunchTime;
    public float punchTimeDifference = 0.01f;
    public float delayForPunchItems = 0.04f;

    private void Update()
    {

    }

    public void ReArrange()
    {
        if (stackItems.Count == 1)
        {
            stackItems[0].isFirst = true;
            stackItems[0].compItemRef = Player.ins.compRefPos;
            stackItems[0].offsetZ = 0f;
        }


        for (int i = 0; i < stackItems.Count - 1; i++)
        {
            if (i == 0)
            {
                stackItems[0].isFirst = true;
                stackItems[0].compItemRef = Player.ins.compRefPos;
                stackItems[0].offsetZ = 0f;
                stackItems[0].offsetZ = 0f;
            }

            stackItems[i + 1].isFirst = false;
            stackItems[i + 1].compItemRef = stackItems[i].transform;
        }
    }

    public void IsActiveClose()
    {
        foreach (var stackItem in stackItems)
            stackItem.isActive = false;
    }

    public void PunchScaleAll()
    {
        if (Time.time - lastPunchTime >= 0.1f)
            StartCoroutine(PunchScaleEffect());
    }

    private IEnumerator PunchScaleEffect()
    {
        lastPunchTime = Time.time;

        for (int i = stackItems.Count - 1; i >= 0; i--)
        {
            yield return new WaitForSeconds(delayForPunchItems);
            stackItems[i].ScalePunch();
        }
    }
}
