using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using _Code.StarterPack.Camera;
using _Code.StarterPack.MainLevel;

public class Finish : MonoBehaviour
{

    #region --------------------| Ins

    private static Finish _ins;

    public static Finish ins
    {
        get
        {
            if (_ins == null)
                _ins = FindObjectOfType<Finish>();
            return _ins;
        }
    }

    #endregion

    [Header("Components")]
    public bool isActive;

    void Start()
    {
        isActive = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isActive)
            return;
        
        if (other.CompareTag("Item")|| other.transform.CompareTag("Player"))
        {
            isActive = false;
            Player.ins.isActive = false;
            CameraManager.Ins.OpenCamera(CameraKey.Camera2);

            DOTween.KillAll();

            Player.ins.Stop();

            StackManager.ins.IsActiveClose();

            LevelManager.Ins.OnWin();
        }
    }

}
