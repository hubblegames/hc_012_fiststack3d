using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MoneyContainer : MonoBehaviour
{

    [Header("Components")]
    public TextMeshPro moneyTXT;

    void Start()
    {

    }

    void Update()
    {

    }

    public void FindWorth()
    {
        var collectedMoney = 0;

        foreach (var item in StackManager.ins.stackItems)
            collectedMoney += item.moneyValue;

        UpdateMoneyTXT(collectedMoney);
    }


    public void UpdateMoneyTXT(int collectedMoney)
    {
        moneyTXT.text = collectedMoney.ToString();
    }
}
