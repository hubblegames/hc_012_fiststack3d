using _Code.StarterPack.MainLevel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, ILevelCanStart
{

    #region --------------------| Ins

    private static Player _ins;

    public static Player ins
    {
        get
        {
            if (_ins == null)
                _ins = FindObjectOfType<Player>();
            return _ins;
        }
    }

    #endregion

    [Header("Values")]
    public bool isActive = false;
    public float pathClamp = 4;
    public float speed = 5;
    public float sideSpeed = 6;
    private float firstClickPosX;
    private float lastClickPosX;
    private float offset;
    private bool moveBack;
    private float lastHitTime;
    public float hitTimeDelay = 1.5f;

    [Header("Components")]
    public Transform compRefPos;
    public Animator compAnimator;
    public MoneyContainer compMoneyContainer;

    void Start()
    {
        compAnimator = GetComponent<Animator>();
    }

    public void OnStart()
    {
        isActive = true;
        //compAnimator.SetBool("Running", true);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            LevelManager.Ins.OnLose();
            isActive = false;
        }

        if (!isActive)
            return;


        Move();

        //CheckIsDead() // FOR COMMORTIAL
    }

    public void Move()
    {
        if (Input.GetMouseButtonDown(0))
        {
            firstClickPosX = Camera.main.ScreenToViewportPoint(Input.mousePosition).x;
            offset = transform.localPosition.x;
        }

        if (Input.GetMouseButton(0))
        {
            lastClickPosX = Camera.main.ScreenToViewportPoint(Input.mousePosition).x;
            float clampedSideMove = Mathf.Clamp(((lastClickPosX - firstClickPosX) * sideSpeed + offset), -pathClamp, pathClamp);

            transform.localPosition = new Vector3(clampedSideMove, transform.localPosition.y, transform.localPosition.z);
        }

        if (Input.GetMouseButtonUp(0))
            offset = 0;

        if (moveBack)
        {
            if (Time.time - lastHitTime >= hitTimeDelay)
                moveBack = false;
            transform.Translate(-Vector3.forward * speed / 2 * Time.deltaTime);
        }
        else
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }

    public void CheckIsDead()
    {
        if (StackManager.ins.stackItems.Count <= 1)
        {
            LevelManager.Ins.OnLose();
            isActive = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Item") && StackManager.ins.stackItems.Count == 0)
        {
            ItemBase item = other.GetComponent<ItemBase>();

            if (!item.isActive)
            {
                item.SetRefItem(item, compRefPos);
            }
        }
    }

    public void Stop()
    {
        isActive = false;
    }

    public void MoveBack()
    {
        moveBack = true;
        lastHitTime = Time.time;
    }
}
