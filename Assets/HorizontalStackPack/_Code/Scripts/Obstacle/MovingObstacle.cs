using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MovingObstacle : ObstacleBase
{

    [Header("Component")]
    public Transform compObstacleBody;

    public override void Start()
    {
        //Move();
    }

    public override void Update()
    {

    }

    public void Move()
    {
        DOTween.Sequence()
            .Append(compObstacleBody.DOMoveY(0, 0.2f))
            .Insert(0.15f,compObstacleBody.DOScaleY(0.5f,0.1f))
            .Append(compObstacleBody.DOMoveY(2, 0.3f))
            .Insert(0.45f,compObstacleBody.DOScaleY(1f, 0.1f))
            .OnComplete(Move)
            ;
    }

    public override void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Item"))
        {
            ItemBase itemBase = other.GetComponent<ItemBase>();

            itemBase.Kill();
        }
    }
}
