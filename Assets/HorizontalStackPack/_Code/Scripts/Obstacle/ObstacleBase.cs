using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleBase : MonoBehaviour
{
    [Header("Values")]
    public bool isActive = true;

    public virtual void Start()
    {
        
    }

    public virtual void Update()
    {
        
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Item"))
        {
            ItemBase itemBase = other.GetComponent<ItemBase>();

            itemBase.Kill();
            Player.ins.MoveBack();
        }
    }
}
