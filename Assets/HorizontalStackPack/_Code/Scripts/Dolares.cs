using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class Dolares : MonoBehaviour
{
    [Header("Components")]
    public TextMeshPro compDolaresTXT;

    void Start()
    {
        DOTween.Sequence()
            .AppendInterval(0.8f)
            .AppendCallback(CloseDolares);
    }

    public void SetText(int value)
    {
        compDolaresTXT.text = "+" + value;
    }

    public void CloseDolares()
    {
        Destroy(gameObject);
    }
}
