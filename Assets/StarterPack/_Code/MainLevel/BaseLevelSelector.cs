﻿using UnityEngine;

namespace _Code.StarterPack.MainLevel
{
    public abstract class BaseLevelSelector : MonoBehaviour
    {
        public abstract void OpenLevel(int level);
    }
}