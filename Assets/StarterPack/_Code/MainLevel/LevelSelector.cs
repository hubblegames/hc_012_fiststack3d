﻿using UnityEngine.SceneManagement;

namespace _Code.StarterPack.MainLevel
{
    public class LevelSelector : BaseLevelSelector
    {
        private DataManager _baseDataManager;

        private int LoopStartLevel => DataManager.LoopStartLevel;
        private int MaxLevel => DataManager.MaxLevel;
        private int LoopLength => MaxLevel - LoopStartLevel;

        public void Awake()
        {
            _baseDataManager = DataManager.Ins;
        }

        public override void OpenLevel(int level)
        {
            if (MaxLevel == 1)
            {
                SceneManager.LoadScene(2);
                return;
            }

            if (level > MaxLevel)
            {
                var dif = level - MaxLevel;
                dif %= LoopLength;
                level = (LoopStartLevel - 1) + dif;
            }

            SceneManager.LoadScene(level + 1);
        }
    }
}