﻿
namespace _Code.StarterPack.Camera
{
    public enum CameraKey
    {
        DontChange,
        Default,
        Camera1,
        Camera2,
        Camera3,
        Camera4,
        Camera5,
    }
}
