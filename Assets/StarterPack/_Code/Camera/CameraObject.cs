﻿using _Code.StarterPack.Camera;
using Cinemachine;
using UnityEngine;

namespace Plugins._StarterPack.Code.Scripts.Managers.Camera
{
    [RequireComponent(typeof(CinemachineVirtualCamera))]
    public class CameraObject : MonoBehaviour
    {
        public CameraKey cameraKey;
        public bool isActive;

        [HideInInspector] public CinemachineVirtualCamera virtualCamera;
        private CinemachineBasicMultiChannelPerlin _perlin;

        private void Awake()
        {
            virtualCamera = GetComponent<CinemachineVirtualCamera>();
            _perlin = virtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        }

        public void StartShaking(float amplitudeGain = 0.5f)
        {
            _perlin.m_AmplitudeGain = amplitudeGain;
        }

        public void StopShaking()
        {
            _perlin.m_AmplitudeGain = 0;
        }
    }
}