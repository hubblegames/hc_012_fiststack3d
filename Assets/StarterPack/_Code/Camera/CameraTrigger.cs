﻿using _Code.StarterPack.Camera;
using Plugins._StarterPack.Code.Scripts.Managers.Camera;
using UnityEngine;

namespace Plugins._StarterPack.Camera
{
    [RequireComponent(typeof(Collider))]
    public class CameraTrigger : MonoBehaviour
    {
        public bool triggerWithId;
        public CameraKey cameraKey;
        public CameraObject cameraObject;

        public float delay;
        public string triggerTag;

        private CameraManager _compCameraManager;

        public void Awake()
        {
            _compCameraManager = CameraManager.Ins;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.gameObject.CompareTag(triggerTag)) return;
            if (triggerWithId)
                _compCameraManager.OpenCamera(cameraObject.GetInstanceID());
            else
                _compCameraManager.OpenCamera(cameraKey);
        }
    }
}