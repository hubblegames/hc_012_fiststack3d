﻿using System.Collections.Generic;
using System.Linq;
using Plugins._StarterPack.Code.Scripts.Managers.Camera;
using UnityEngine;

namespace _Code.StarterPack.Camera
{
    public class CameraManager : MonoBehaviour
    {
        private static CameraManager _ins;

        public static CameraManager Ins
        {
            get
            {
                if (_ins == null)
                    _ins = FindObjectOfType<CameraManager>();

                return _ins;
            }
        }
        
        private UnityEngine.Camera _mainCamera;
        private CameraKey _activeKey;
        private int _activeId;

        public Dictionary<CameraKey, CameraObject> camList = new Dictionary<CameraKey, CameraObject>();
        public Dictionary<int, CameraObject> camListId = new Dictionary<int, CameraObject>();

        private CameraObject ActiveCamera { get; set; }

        public  UnityEngine.Camera MainCamera => _mainCamera;

        private void Awake()
        {
            _ins = this;
            _mainCamera = FindObjectOfType<UnityEngine.Camera>();
        }

        public void Start()
        {
            foreach (var curCamera in FindObjectsOfType<CameraObject>())
            {
                if (ActiveCamera == null || ActiveCamera.virtualCamera.Priority < curCamera.virtualCamera.Priority)
                {
                    ActiveCamera = curCamera;
                    _activeKey = curCamera.cameraKey;
                    _activeId = curCamera.GetInstanceID();
                }

                // --- Camera Key
                if (!camList.ContainsKey(curCamera.cameraKey))
                    camList.Add(curCamera.cameraKey, curCamera);
                else
                    camList[curCamera.cameraKey] = curCamera;

                // --- Camera Id
                var curId = curCamera.GetInstanceID();
                if (!camListId.ContainsKey(curId))
                    camListId.Add(curId, curCamera);
                else
                    camListId[curId] = curCamera;
            }

            foreach (var cam in camList.Where(cam => cam.Value.cameraKey != _activeKey))
            {
                cam.Value.virtualCamera.Priority = 0;
                cam.Value.isActive = false;
            }
        }

        public  void OpenCamera(CameraKey cameraKey)
        {
            if (cameraKey == CameraKey.DontChange || cameraKey == _activeKey || !camList.ContainsKey(cameraKey))
                return;

            if (!camList.ContainsKey(cameraKey)) return;

            CloseCamera(_activeKey);
            _activeKey = cameraKey;
            ActiveCamera = camList[_activeKey];
            _activeId = ActiveCamera.GetInstanceID();

            ActiveCamera.virtualCamera.Priority = 10;
        }

        public  void CloseCamera(CameraKey cameraKey)
        {
            if (cameraKey != CameraKey.DontChange)
                camList[_activeKey].virtualCamera.Priority = 0;
        }

        public  void OpenCamera(int cameraId)
        {
            if (cameraId == _activeId || !camListId.ContainsKey(cameraId)) return;

            CloseCamera(_activeId);
            _activeId = cameraId;
            ActiveCamera = camListId[_activeId];

            _activeKey = ActiveCamera.cameraKey;
            ActiveCamera.virtualCamera.Priority = 10;
        }

        public  void CloseCamera(int cameraId)
        {
            if (!camListId.ContainsKey(cameraId)) return;

            camListId[cameraId].virtualCamera.Priority = 0;
        }

        public  void StartShaking(float amplitudeGain)
        {
            ActiveCamera.StartShaking(amplitudeGain);
        }

        public  void StopShaking()
        {
            ActiveCamera.StopShaking();
        }

    }
}