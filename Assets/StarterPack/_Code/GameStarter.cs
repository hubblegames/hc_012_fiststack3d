﻿using _Code.StarterPack.MainLevel;
using UnityEngine;
using Tabtale.TTPlugins;

namespace _Code
{
    public class GameStarter : MonoBehaviour
    {
        private BaseLevelSelector _levelSelectable;
        private DataManager _baseDataManager;

        private void Awake()
        {
            TTPCore.Setup();
        }

        private void Start()
        {
            _levelSelectable = GetComponent<BaseLevelSelector>();
            _baseDataManager = GetComponent<DataManager>();
            DataManager.StarterScene = true;
            _levelSelectable.OpenLevel(DataManager.Level);
            
        }
    }
}