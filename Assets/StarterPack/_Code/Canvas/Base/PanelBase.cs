﻿using System;
using Code.Scripts.Manager.Canvas;
using Code.Scripts.Manager.Canvas.Enum;
using DG.Tweening;
using Plugins._StarterPack.Code.Canvas.CustomPanels;
using UnityEngine;
using UnityEngine.Events;

namespace Plugins._StarterPack.Code.Canvas.Base
{
    public class PanelBase : MonoBehaviour
    {
        private BaseCanvasManager _baseCanvasManager;

        [Header("Panel Info")] public CanvasId panelId;
        public bool isOpen;
        public bool startPanel;
        public AnimationType openAnimationType = AnimationType.FadeIn;
        public AnimationType loseAnimationType = AnimationType.FadeOut;

        [Header("Panel Components")] private CanvasGroup _compCanvasGroup;
        private RectTransform _compRectTransform;

        [Header("Panel Events")] public UnityEvent onOpenEvents;
        public UnityEvent onCloseEvents;

        [Header("Screen Values")] private float _screenWıdth;
        private float _screenHeıght;
        private Vector2 _screenCenter;

        public virtual void Start()
        {
            _baseCanvasManager = BaseCanvasManager.Ins;

            _screenWıdth = Screen.width;
            _screenHeıght = Screen.height;
            _screenCenter = new Vector2(_screenWıdth / 2, _screenHeıght / 2);

            _compCanvasGroup = GetComponent<CanvasGroup>();
            _compRectTransform = GetComponent<RectTransform>();
        }

        public virtual void OnOpen()
        {
            isOpen = true;
            onOpenEvents.Invoke();
            DOTween.Play(panelId);
        }

        public virtual void OnClose()
        {
            isOpen = false;
            onCloseEvents.Invoke();
            DOTween.Kill(panelId);
        }

        public virtual void OpenPanelById(CanvasId panelName)
        {
            _baseCanvasManager.OpenPanelById(panelName);
        }

        public virtual void ClosePanelById(CanvasId panelName)
        {
            _baseCanvasManager.ClosePanelById(panelName);
        }

        public static void PanelSetActive(PanelBase panel, bool value)
        {
            var panelCanvasGroup = panel._compCanvasGroup;
            panelCanvasGroup.alpha = value ? 1 : 0;
            panelCanvasGroup.blocksRaycasts = value;
        }

        public void PlayAnimation(PanelBase selectedPanel, AnimationType curAnimation, float duration = 0.3f)
        {
            var canvasGroup = selectedPanel._compCanvasGroup;
            var rectTransform = selectedPanel._compRectTransform;

            switch (curAnimation)
            {
                case AnimationType.None:
                    PanelSetActive(selectedPanel, true);
                    break;
                
                case AnimationType.FadeIn:
                    rectTransform.position = _screenCenter;
                    rectTransform.FadeIn(duration, canvasGroup)
                        .OnComplete(() => PanelSetActive(selectedPanel, true));
                    break;
                
                case AnimationType.FadeOut:
                    rectTransform.position = _screenCenter;
                    rectTransform.FadeOut(duration, canvasGroup)
                        .OnComplete(() => PanelSetActive(selectedPanel, false));
                    break;
                
                case AnimationType.SlideInDown:
                    PanelSetActive(selectedPanel, true);
                    rectTransform.position = new Vector2(_screenCenter.x, -(_screenHeıght - _screenCenter.y));
                    rectTransform.SlideInCenter(duration, _screenCenter);
                    break;
                
                case AnimationType.SlideOutDown:
                    rectTransform.SlideOutDown(duration, _screenCenter, _screenHeıght)
                        .OnComplete(() => PanelSetActive(selectedPanel, false));
                    break;
                
                case AnimationType.SlideInUp:
                    PanelSetActive(selectedPanel, true);
                    rectTransform.position = new Vector2(_screenCenter.x, _screenHeıght + _screenCenter.y);
                    rectTransform.SlideInCenter(duration, _screenCenter);
                    break;
                
                case AnimationType.SlideOutUp:
                    rectTransform.SlideOutUp(duration, _screenCenter, _screenHeıght)
                        .OnComplete(() => PanelSetActive(selectedPanel, false));
                    break;
                
                case AnimationType.SlideInLeft:
                    PanelSetActive(selectedPanel, true);
                    rectTransform.position = new Vector2(-_screenWıdth + _screenCenter.x, _screenCenter.y);
                    rectTransform.SlideInCenter(duration, _screenCenter);
                    break;
                
                case AnimationType.SlideOutLeft:
                    rectTransform.SlideOutLeft(duration, _screenCenter, _screenWıdth)
                        .OnComplete(() => PanelSetActive(selectedPanel, false));
                    break;
                
                case AnimationType.SlideInRight:
                    PanelSetActive(selectedPanel, true);
                    rectTransform.position = new Vector2(_screenWıdth + _screenCenter.x, _screenCenter.y);
                    rectTransform.SlideInCenter(duration, _screenCenter);
                    break;

                case AnimationType.SlideOutRight:
                    rectTransform.SlideOutRight(duration, _screenCenter, _screenWıdth)
                        .OnComplete(() => PanelSetActive(selectedPanel, false));
                    break;

                case AnimationType.ScaleUp:
                    PanelSetActive(selectedPanel, true);
                    rectTransform.ScaleUp(duration);
                    break;
                
                case AnimationType.ScaleDown:
                    rectTransform.ScaleDown(duration)
                        .OnComplete(() => PanelSetActive(selectedPanel, false));
                    break;
                
                default:
                    throw new ArgumentOutOfRangeException(nameof(curAnimation), curAnimation, null);
            }
        }

        public void CloseYourSelf()
        {
            ClosePanelById(panelId);
        }

        public void OpenYourSelf()
        {
            OpenPanelById(panelId);
        }
    }
}