﻿using Code.Scripts.Manager.Canvas.Enum;
using UnityEngine;

namespace Plugins._StarterPack.Code.Canvas.Base
{
    public abstract class BaseCanvasManager : MonoBehaviour
    {
        private static BaseCanvasManager _ins;

        public static BaseCanvasManager Ins
        {
            get
            {
                if (_ins == null)
                    _ins = FindObjectOfType<BaseCanvasManager>();

                return _ins;
            }
        }    
        
        public abstract void CloseAllPanels();
        public abstract void ClosePanelById(CanvasId id);
        public abstract void OpenOnlyOne(CanvasId id, bool closeConst = false);
        public abstract void OpenPanelById(CanvasId id);
        public abstract PanelBase FindPanel(CanvasId id);

        public abstract TModel GetPanel<TModel>();
        
        public TPanelType FindPanel<TPanelType>() where TPanelType : MonoBehaviour
        {
            return GetComponentInChildren<TPanelType>();
        }

        public TComponentType FindComponentInPanel<TPanelType, TComponentType>() where TPanelType : MonoBehaviour
            where TComponentType : MonoBehaviour
        {
            return FindPanel<TPanelType>().GetComponentInChildren<TComponentType>();
        }
        
        public TComponentType[] FindComponentsInPanel<TPanelType, TComponentType>() where TPanelType : MonoBehaviour
            where TComponentType : MonoBehaviour
        {
            return FindPanel<TPanelType>().GetComponentsInChildren<TComponentType>();
        }
    }
}