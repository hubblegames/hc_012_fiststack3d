﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Code.Scripts.Manager.Canvas;
using Code.Scripts.Manager.Canvas.Enum;
using UnityEngine;

namespace Plugins._StarterPack.Code.Canvas.Base
{
    public class SimpleCanvasManager : BaseCanvasManager
    {
        [Header("Panels")] [HideInInspector] public List<PanelBase> panels = new List<PanelBase>();

        private void Awake()
        {
            panels.AddRange(GetComponentsInChildren<PanelBase>());
        }

        private IEnumerator Start()
        {
            yield return new WaitForEndOfFrame();
            CloseAllPanels();
        }

        public override void OpenPanelById(CanvasId panelName)
        {
            var panel = FindPanel(panelName);

            if (panel.isOpen) return;

            panel.OnOpen();
            panel.PlayAnimation(panel, panel.openAnimationType);
        }

        public override void ClosePanelById(CanvasId panelName)
        {
            var panel = FindPanel(panelName);
            panel.OnClose();
            panel.PlayAnimation(panel, panel.loseAnimationType);
        }

        public override TModel GetPanel<TModel>()
        {
            return GetComponentInChildren<TModel>();
        }

        public override PanelBase FindPanel(CanvasId id)
        {
            return panels.Single(p => p.panelId == id);
        }

        public override void OpenOnlyOne(CanvasId id, bool closeConst = false)
        {
            if (id == CanvasId.Const && closeConst)
                foreach (var panel in panels)
                    ClosePanelById(panel.panelId);
            else
                foreach (var panel in panels.Where(s => s.panelId != CanvasId.Const))
                    ClosePanelById(panel.panelId);
        }

        public override void CloseAllPanels()
        {
            foreach (var panel in panels)
            {
                if (!panel.startPanel)
                {
                    ClosePanelById(panel.panelId);
                }
                else
                {
                    panel.OnOpen();
                    PanelBase.PanelSetActive(panel, true);
                }
            }
        }
      
    }
}