﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;
using Slider = UnityEngine.UI.Slider;

namespace Plugins._StarterPack.Code.Canvas.Components.LevelComponent
{
    [ExecuteAlways]
    public class ProgressBar : MonoBehaviour
    {
        public Image compSlider;
        public RectTransform compCursor;
        [Range(0, 1)] public float value;
        
        protected float OffsetX;
        protected float Width;

        protected IEnumerator Start()
        {
            yield return new WaitForEndOfFrame();
            Width = compSlider.GetComponent<RectTransform>().rect.width;
            OffsetX = compCursor.rect.width / 2f;
        }

        private void UpdateProgress(float progress)
        {
            compSlider.fillAmount = progress;
            compCursor.anchoredPosition = new Vector2(Width * progress - OffsetX, 0);
        }
         
    }
}