﻿namespace Code.Scripts.Manager.Canvas.Enum
{
    public enum AnimationType
    {
        None,
        FadeIn,
        FadeOut,
        SlideInLeft,
        SlideOutLeft,
        SlideInRight,
        SlideOutRight,
        SlideInUp,
        SlideOutUp,
        SlideInDown,
        SlideOutDown,
        ScaleUp,
        ScaleDown
    }

    public enum CanvasId
    {
        Start,
        InGame,
        Skin,
        Settings,
        Win,
        Lose,
        Const,
        ThrowGame,
        MatchMaking,
        StartGame,
        Tutorial
    }
}
