﻿using Plugins._StarterPack.Code.Canvas.Base.Panels;
using UnityEngine;

namespace Plugins._StarterPack.Code.Canvas.CustomPanels
{
    public class SettingsPanel : MonoBehaviour
    {
        private FixedPanel _fixedPanel;

        public void Start()
        {
            _fixedPanel = GetComponent<FixedPanel>();

            _fixedPanel.onOpenEvents.AddListener(OnOpen);
            _fixedPanel.onCloseEvents.AddListener(OnClose);
        }

        private static void OnOpen()
        {
        }

        private static void OnClose()
        {
        }

        public void PrivacyPolicy()
        {
            Debug.Log("PrivacyPolicy");
            //TODO: PrivacyPolicy 
        }
    }
}