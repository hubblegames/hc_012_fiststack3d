﻿using _Code.StarterPack.MainLevel;
using UnityEngine;

namespace _Code.StarterPack.Canvas.CustomPanels
{
    public class LosePanel : MonoBehaviour
    {
 
        public void BTN_Restart()
        {
            LevelManager.Ins.OnRestartLevel();
        }
        
    }
}