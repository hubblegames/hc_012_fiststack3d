﻿using _Code.StarterPack.MainLevel;
using DG.Tweening;
using Plugins._StarterPack.Code.Canvas.CustomPanels;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace _Code.StarterPack.Canvas.CustomPanels
{
    public class WinPanel : MonoBehaviour
    {
        [SerializeField] private ConstPanel compConstPanel;

        [Header("Particles")] [SerializeField] private ParticleSystem compConfetti1;
        [SerializeField] private ParticleSystem compConfetti2 = null;

        [Header("Dollar Components")]
        [SerializeField]
        private TextMeshProUGUI compDollarText;

        [SerializeField] private Transform compDollarIcon;
        [SerializeField] private Transform compConstDollarIcon;
        [SerializeField] private GameObject prefDollar;

        [Header("Prize Components")]
        [SerializeField]
        private CanvasGroup compPrizeContainer;

        [SerializeField] private TextMeshProUGUI compPrizePercentText;
        [SerializeField] private Image compPrizePercentImage;

        [Header("Continue Components")]
        [SerializeField]
        private Button compContinueBtn;

        [SerializeField] private Image compContinueBtnImg;

        private float _prizeStartValue;

        public void SetStartValues(int increaseCoin, float startValue)
        {
            // --- Set Coin
            compDollarText.text = "+" + increaseCoin;
            _prizeStartValue = startValue;

            compPrizePercentText.text = "%" + _prizeStartValue;
            compPrizePercentImage.fillAmount = _prizeStartValue * 0.01f;
        }

        public void OnOpen(float prizeIncrease, int targetCoin)
        {
            DOTween.Sequence()
                .AppendInterval(0.5F)
                .AppendCallback(() =>
                {
                    PlayConfetti();
                    InstantiateCoin(targetCoin);
                })
                .AppendInterval(2f)
                .OnComplete(() => UpdatePrizePercent(prizeIncrease));
        }


        private void PlayConfetti()
        {
            compConfetti1.Play();
            compConfetti2.Play();
        }

        private void UpdatePrizePercent(float increase, float duration = 1)
        {
            DOTween.Sequence()
                .Append(compPrizeContainer.DOFade(1f, 0.2f))
                .Append(compPrizeContainer.transform.DOScale(Vector3.one * 1.4f, 0.2f))
                .Append(compPrizeContainer.transform.DOScale(Vector3.one, 0.2f))
                .AppendCallback(() =>
                {
                    var startValue = _prizeStartValue;
                    var endValue =
                        (int) (startValue + increase);

                    DOTween.To(() => startValue, x => startValue = x, endValue, duration)
                        .OnUpdate(() =>
                        {
                            compPrizePercentImage.fillAmount = startValue * 0.01f;
                            compPrizePercentText.text = "%" + (int) startValue;
                        });
                }).OnComplete(OpenContinue_BTN);
        }

        private void InstantiateCoin(int targetCoin)
        {
            const int turn = 20;
            var seq = DOTween.Sequence();

            compConstPanel.UpdateCoinAnimation(turn * 0.1f + 0.8f, targetCoin);

            for (var i = 0; i < turn; i++)
            {
                seq.AppendInterval(0.1f)
                    .AppendCallback(() =>
                    {
                        Vector3 offset = Random.insideUnitCircle * 50;
                        var position = compConstDollarIcon.position;

                        var dollarComp = Instantiate(prefDollar, compDollarIcon.position,
                                Quaternion.identity, compConstDollarIcon.parent)
                            .transform;

                        DOTween.Sequence()
                            // --- Scale
                            .Append(dollarComp.DOMove(dollarComp.position + offset, 0.2f))
                            .Join(dollarComp.DOScale(Vector3.one, 0.2f))
                            // --- Move
                            .Append(dollarComp.DOMove(position, 0.6f).SetEase(Ease.InCubic))
                            .Append(dollarComp.DOScale(Vector3.zero, 0.2f))
                            .OnComplete(() => Destroy(dollarComp.gameObject))
                            ;
                    });
            }
        }


        public void BTN_Continue()
        {
            LevelManager.Ins.OnNextLevel();
        }

        private void OpenContinue_BTN()
        {
            compContinueBtn.interactable = true;
            compContinueBtnImg.DOFade(1, 1f);
        }
    }
}