﻿using _Code.StarterPack.MainLevel;
using Code.Scripts.Manager.Canvas.Enum;
using DG.Tweening;
using Plugins._StarterPack.Code.Canvas.Base.Panels;
using UnityEngine;

namespace Plugins._StarterPack.Code.Canvas.CustomPanels
{
    public class StartPanel : MonoBehaviour
    {
        // --- Components
        [Header("UI Items")] public Transform compStartBtn;
        private FixedPanel _fixedPanel;

        public void Start()
        {
            _fixedPanel = GetComponent<FixedPanel>();

            _fixedPanel.onOpenEvents.AddListener(OnOpen);
            _fixedPanel.onCloseEvents.AddListener(OnClose);
        }

        private void OnOpen()
        {
            BounceText();
        }

        private static void OnClose()
        {
        }

        private void BounceText()
        {
            DOTween.Sequence()
                .SetId(_fixedPanel.panelId)
                .Append(compStartBtn.DOScale(Vector3.one * 1.1f, 1f).SetEase(Ease.InBounce))
                .Append(compStartBtn.DOScale(Vector3.one, 1f).SetEase(Ease.OutBounce))
                .AppendCallback(BounceText);
        }

        public void BTN_StartBehavior()
        {
            _fixedPanel.CloseYourSelf();
            LevelManager.Ins.OnStart();
        }

        public void OpenSettingsPanel()
        {
            _fixedPanel.OpenPanelById(CanvasId.Settings);
        }
    }
}