﻿using DG.Tweening;
using Plugins._StarterPack.Code.Canvas.Base.Panels;
using UnityEngine;
using UnityEngine.UI;

namespace Plugins._StarterPack.Code.Canvas.CustomPanels
{
    public class ThrowGamePanel : MonoBehaviour
    {
        public Slider slider;
        public FixedPanel fixedPanel;

        void Start()
        {
            fixedPanel = GetComponent<FixedPanel>();
        }

        void Update()
        {
        }

        public void UpdateRedPlayerSlider(float value)
        {
            slider.DOValue(value, 0.5f);
        }
    }
}