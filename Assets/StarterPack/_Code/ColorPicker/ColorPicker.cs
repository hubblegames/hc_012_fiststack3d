﻿using _Code.StarterPack.MainLevel;
using UnityEngine;

namespace _Code.StarterPack.ColorPicker
{
    public class ColorPicker : MonoBehaviour
    {
        public Material[] materials;
        private MeshRenderer _compRenderer;

        private void Start()
        {
            var level = DataManager.Level;
            _compRenderer = GetComponent<MeshRenderer>();
            _compRenderer.material = materials[level % materials.Length];
        }
    }
}