﻿public enum TutorialAnimationType
{
    TapAndHold,
    TapAndDrag,
    Release,
}
