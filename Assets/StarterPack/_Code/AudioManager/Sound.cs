﻿using UnityEngine.Audio;
using UnityEngine;

//custom bir class yazılınca inspectorde görünmesi için system.serializable kullanmak zorundasın
[System.Serializable]
public class Sound {

	public string name;

	public AudioClip clip;

	[Range(0f,1f)]
	public float volume;
	[Range(.1f,3f)]
	public float pitch;

	public bool loop;

	public float lenght;

	[HideInInspector]
	public AudioSource source;

}
